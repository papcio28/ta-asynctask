package com.urban.threads;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.ProgressBar;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    public static final String KEY_PROGRESS = "progress";
    @BindView(R.id.progress)
    protected ProgressBar mProgress;
    @BindView(R.id.start_btn)
    protected Button mStartButton;
    private CountdownAsyncTask mCountdownAsyncTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey(KEY_PROGRESS)) {
            int currentProgress = savedInstanceState.getInt(KEY_PROGRESS);
            startNewCountdown(currentProgress);
        }
    }

    @OnClick(R.id.start_btn)
    protected void onClickStart() {
        startNewCountdown(1);
    }

    private void startNewCountdown(int currentProgress) {
        mCountdownAsyncTask = new CountdownAsyncTask(mProgress, mStartButton, currentProgress);
        mCountdownAsyncTask.execute();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mCountdownAsyncTask != null && mCountdownAsyncTask.getStatus() != AsyncTask.Status.FINISHED) {
            outState.putInt(KEY_PROGRESS, mProgress.getProgress());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCountdownAsyncTask != null) {
            mCountdownAsyncTask.cancel(false);
        }
    }

    public static class CountdownAsyncTask extends AsyncTask<Void, Integer, Void> {
        private static final String TAG = "CountdownAsyncTask";
        private ProgressBar mProgressBar;
        private Button mButton;
        private int mStartValue;

        public CountdownAsyncTask(ProgressBar progressBar, Button button, int currentProgress) {
            mProgressBar = progressBar;
            mButton = button;
            mStartValue = currentProgress;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // Wykonuje się w wątku w tle
            Random random = new Random();
            for (int i = mStartValue; i <= 100; i++) {
                if (isCancelled()) {
                    break;
                }
                Log.d(TAG, String.valueOf(i));
                publishProgress(i);
                try {
                    Thread.sleep(random.nextInt(350) + 50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            // Przygotowanie do wykonania zadania w tle, wykonywane na wątku UI
            super.onPreExecute();
            mProgressBar.setProgress(0);
            mButton.setEnabled(false);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            // Po wykonaniu zadania w tle, wykonywane na wątku UI
            super.onPostExecute(aVoid);
            mButton.setEnabled(true);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            // Opublikowanie postępów prac w wątku w tle, wykonywane na wątku UI
            super.onProgressUpdate(values);
            mProgressBar.setProgress(values[0]);
        }
    }
}
























